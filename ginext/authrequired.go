package ginext

import (
	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	. "gitlab.com/jx-sharing/go-common/common"
	"net/http"
	"strconv"
)

// AuthRequiredMiddleware is required the request has to have x-user-id in header
// (it usually set by API Gateway)
func AuthRequiredMiddleware(c *gin.Context) {
	headers := struct {
		UserID   string  `header:"x-user-id" validate:"required"`
		UserMeta string `header:"x-user-meta"`
		TenantID uint   `header:"x-tenant-id"`
	}{}
	if c.ShouldBindHeader(&headers) != nil {
		_ = c.Error(NewError(http.StatusUnauthorized, "unauthorized"))
		c.Status(http.StatusUnauthorized)
		c.Abort()
		return
	}

	c.Set(HeaderUserID, headers.UserID)
	c.Set(HeaderUserMeta, headers.UserMeta)
	c.Set(HeaderTenantID, headers.TenantID)

	c.Next()
}

func UintHeaderValue(c *gin.Context, headerName string) uint {
	sValue := c.GetHeader(headerName)
	if sValue == "" {
		return 0
	}
	v, err := strconv.Atoi(sValue)
	if err != nil {
		return 0
	}

	return uint(v)
}

func UintUserID(c *gin.Context) uint {
	return UintHeaderValue(c, HeaderUserID)
}

func UintTenantID(c *gin.Context) uint {
	return UintHeaderValue(c, HeaderTenantID)
}

type GetStringer interface {
	GetString(key string) string
}

func GetUserID(c GetStringer) string {
	return c.GetString(HeaderUserID)
}

// GetUserID returns the user ID embedded in Gin context
func GetUUIDUserID(c *gin.Context) (uuid.UUID, error) {
	tID, exists := c.Get(HeaderUserID)
	if !exists {
		return uuid.Nil, c.Error(NewError(http.StatusUnauthorized, "unauthorized, userID is empty"))
	}
	tmp, _ := tID.(string)
	ID, err := uuid.Parse(tmp)
	if err != nil {
		return uuid.Nil, c.Error(NewError(http.StatusUnauthorized, "unauthorized, userID expected uuid type"))
	}
	return ID, nil
}
