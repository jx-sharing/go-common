package migration

import (
	"database/sql"
	"fmt"

	"github.com/golang-migrate/migrate/v4"
	"github.com/golang-migrate/migrate/v4/database"
	"github.com/golang-migrate/migrate/v4/database/postgres"
	_ "github.com/golang-migrate/migrate/v4/database/postgres"
	_ "github.com/golang-migrate/migrate/v4/source/file"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/stdlib"
	"gitlab.com/jx-sharing/go-common/db"
	"gitlab.com/jx-sharing/go-common/logger"
)

type migrateWrapper struct {
	m *migrate.Migrate
}

var wrapper *migrateWrapper

func NewMigrate(cfg *db.Config) *migrateWrapper {
	if wrapper != nil {
		return wrapper
	}

	connectionString := fmt.Sprintf("postgres://%s:%s@%s:%s/%s?sslmode=disable",
		cfg.User,
		cfg.Pass,
		cfg.Host,
		cfg.Port,
		cfg.Name,
	)
	connConfig, _, driver := OpenDB(connectionString)

	m, err := migrate.NewWithDatabaseInstance(
		fmt.Sprintf("file://%s", cfg.MigrationsPath),
		connConfig.Database,
		driver,
	)
	if err != nil {
		logger.Tag("migration").Fatalf("Error creating db migrate svc: %s", err)
	}
	wrapper = &migrateWrapper{m}

	return wrapper
}

func OpenDB(connectionStr string) (*pgx.ConnConfig, *sql.DB, database.Driver) {
	connConfig, err := pgx.ParseConfig(connectionStr)
	if err != nil {
		logger.Tag("migration").Fatalf("Error parsing DSN svc: %s", err)
	}

	conn := stdlib.OpenDB(*connConfig)
	driver, err := postgres.WithInstance(conn, &postgres.Config{})
	if err != nil {
		logger.Tag("migration").Fatalf("Error creating db migrate svc: %s", err)
	}
	return connConfig, conn, driver
}

func (w *migrateWrapper) Up() error {
	// keep old version in case of failed we need to restore back to it
	oldversion, dirty, err := w.m.Version()
	if err != nil && err != migrate.ErrNilVersion {
		logger.Tag("migration").Errorln("Unable to fetch current migration version", err)
		return err
	}

	logger.Tag("migration").Infof("Current migrate version=%d - is dirty=%v", oldversion, dirty)

	// start migration
	err = w.m.Up()
	if err != nil {
		if err == migrate.ErrNoChange {
			logger.Tag("migration").Info("No migration detected")
		} else {
			// restore DB to last working version
			fErr := w.m.Force(int(oldversion))
			if fErr != nil {
				logger.Tag("migration").Errorln("Unable to force set back to old migration version", err)
			}
			return err
		}
	}
	return nil
}

func (w *migrateWrapper) Steps(steps int) error {
	return w.m.Steps(steps)
}
