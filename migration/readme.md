# Migration

This package provide a way to migration postgresql DB from script

## Configuration

`Config` struct provides a template for database configuration, it also has annotation for loading variables
from environment.

`migrationsPath` config must be set in config in order to check and migration

## Get started

```go
package main

import (
  "gitlab.com/jx-sharing/go-common/migration"
	"gitlab.com/jx-sharing/go-common/logger"
)

func main() {
  // other config like host, port, user, pass, db_name is required
  config := &db.Config{Driver: "postgres", MigrationsPath: "./migrations"}

  logger.Tag("migration").Info("Execute data base migrations")
	if err := migration.NewMigrate(&config).Up(); err != nil {
		logger.Tag("migration").Fatalf("Error migrating database: %s", err)
		panic(err)
	}

  // ... init your API service here
}
```

Create migration script following this format:
`2021071100_init_schema.up.sql`
`2021071100_init_schema.down.sql`

- 2021071100: date in format yyyymmddvv. with yyyy = year, mm = month, dd = day and vv = version on that day

```postgresql

BEGIN;

# your postgresql script

COMMIT;
```
